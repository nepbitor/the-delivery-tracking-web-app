<?php

namespace App\Http\Controllers;

use App\Delivery;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{


    // Landing page
    public function index()
    {
        $data = ['title' => 'Delivery Tracker'];

        return view('index', $data);
    }

    // Get delivery date
    public function getDeliveryDate($code)
    {
        $delivery = Delivery::select('delivery_date')
                    ->where('tracking_code', $code)->first();

        $delivery_date = $delivery ? date('M d, Y @ h:i:s A', strtotime($delivery->delivery_date)) : '';

        return response()->json($delivery_date, 201);
    }
}