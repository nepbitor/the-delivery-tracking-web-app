## Summary

A simple Laravel/Lumen Framework system where people can enter a shipping tracking code in a form and as a result they will see an estimated delivery date. The frontend is supposed to be connecting to the backend through a REST API.

---

## Setup

1. Open your terminal/cmd inside the root folder.
2. Create sqlite file by running this command:
**touch database/database.sqlite**
3. Execute migration and seeding by running this command:
**php artisan migrate:refresh --seed**
4. Run localhost using this command:
**php -S localhost:8000 -t public**


Fixtures:

| tracking_code | delivery_date       |

| 111           | 2019-08-11 12:00:00 |

| 222           | 2019-10-02 14:00:00 |

| 333           | 2019-09-13 23:00:00 |

| 4404          | 2020-01-24 23:00:00 |

| 5505          | 2020-02-15 23:00:00 |
