<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<!-- Validator CSS -->
	<link rel="stylesheet" href="{{ url('assets/form-validator/theme-default.min.css') }}">

	<!-- Custom Style -->
	<link rel="stylesheet" href="{{ url('assets/css/styles.css') }}">

	<title>{{ $title }}</title>
</head>
<body class="text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-4 offset-md-4 pt-5">
				<h1 class="h3 mb-3 font-weight-normal">{{ $title }}</h1>
				<form id="form-tracker" class="justify-content-center align-items-center toggle-disabled" method="POST" action="{{ url('api/track') }}">
					<div class="form-group">
						<label for="tracking_code" class="sr-only">Tracking #</label>
						<input type="text" id="tracking_code" name="tracking_code" class="form-control" placeholder="Enter tracking number..." data-validation="required number length" data-validation-length="min3">
					</div>
					<button id="btn-track" type="submit" class="btn btn-primary w-100">TRACK</button>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="trackerModal" tabindex="-1" role="dialog" aria-labelledby="trackerModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Result</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- Validator JS -->
	<script type="text/javascript" src="{{ url('assets/form-validator/jquery.form-validator.min.js') }}"></script>

	<!-- Custom Script -->
	<script src="{{ url('assets/js/scripts.js') }}"></script>

</body>
</html>