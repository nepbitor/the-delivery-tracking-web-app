(function($) {
	$.validate({
		modules : 'toggleDisabled',
		disabledFormFilter : 'form.toggle-disabled',
	});

	$('#form-tracker')
		.off('submit')
		.on('submit', function(e) {
			e.preventDefault();
			var _this = $(this),
				_btn = $('#btn-track'),
				loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Loading...',
				_trackerModal = $("#trackerModal");

			_trackerModal.on('hide.bs.modal', function(e) {
				$('#tracking_code').val('');
			});

			if ($(this).html() !== loadingText) {
				_btn.data('original-text', _btn.html());
				_btn.html(loadingText);
				_btn.attr('disabled', true);
			}

			$.post(_this.attr('action') + '/' + $('#tracking_code').val(), function(delivery_date) {
				_trackerModal
					.on('show.bs.modal', function(e) {

						if (delivery_date !== '')
							$msg = 'Your delivery date is on ' + delivery_date;
						else
							$msg = 'Delivery code not found!';

						$('.modal-body', this).text($msg);
					})
					.modal('show');

			}).done(function() {
				_btn.html(_btn.data('original-text'));
				_btn.removeAttr('disabled');
			});
		});
})(jQuery);